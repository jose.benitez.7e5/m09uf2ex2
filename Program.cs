﻿using System;
using System.Threading;

namespace M09UF2E2
{
    class Program
    {
        static void Main()
        {
            string frase = "Una vegada hi havia un gat\n";
            string frase1 = "En un lugar de la Mancha\n";
            string frase2 = "Once upon a time in the west\n";

            Thread thread1 = new Thread(() =>
            {
                SlowWriteLine(frase);
            });
            Thread thread2 = new Thread(() =>
            {
                thread1.Join();
                SlowWriteLine(frase1);
            });
            Thread thread3 = new Thread(() =>
            {
                thread2.Join();
                SlowWriteLine(frase2);
            });

            thread1.Start(); thread2.Start(); thread3.Start();

            Nevera nevera = new Nevera(5);

            Thread thread4 = new Thread(() =>
            {
                Thread.Sleep(10000);
                thread3.Join();
                nevera.OmplirNevera("Anitta");


            });
            Thread thread5 = new Thread(() =>
            {
                Thread.Sleep(1000);
                thread4.Join();
                nevera.BeureCervesa("Bad Bunny");


            });
            Thread thread6 = new Thread(() =>
            {
                Thread.Sleep(1000);
                thread5.Join();
                nevera.BeureCervesa("Lil Nas X");

            });
            Thread thread7 = new Thread(() =>
            {
                Thread.Sleep(1000);
                thread6.Join();
                nevera.BeureCervesa("Manuel Turizo");

            });

            thread4.Start(); thread5.Start(); thread6.Start(); thread7.Start();
        }

        private static void SlowWriteLine(string frase)
        {
            for (int i = 0; i < frase.Length; i++)
            {
                Console.Write(frase[i]);
                if (frase[i] == ' ')
                {
                    Thread.Sleep(500);
                }
            }
            Thread.Sleep(500);
        }

    }

    class Nevera
    {

        public int beer = 0;

        public Nevera(int cerveses) => this.beer = cerveses;

        public int OmplirNevera(string name)
        {

            Random randomnum = new Random();
            int fillbeer = randomnum.Next(0, 7);
            beer += fillbeer;
            if (beer > 9)
                beer = 9;
            Console.WriteLine(name + " ha omplert " + fillbeer + " cerveses");
            Console.WriteLine("Hi ha " + beer + " birres a la nevera.");
            return beer;
        }

        public int BeureCervesa(string name)
        {
            Random randomnum = new Random();
            int drinkbeer = randomnum.Next(0, 7);
           

            if (drinkbeer > beer)
            {
                if (beer == 0)
                    Console.WriteLine(name + " no s'ha pogut beure cap cerveza perque no hi ha més");
                else
                Console.WriteLine(name + " volia beure " + drinkbeer + " pero només ha pogut beure " + beer);
                beer = 0;
                Console.WriteLine("Hi ha " + beer + " birres a la nevera.");
            }
            else if (drinkbeer <= beer)
            {
                beer -= drinkbeer;
                Console.WriteLine(name + " ha begut " + drinkbeer + " cerveses");
                Console.WriteLine("Hi ha " + beer + " birres a la nevera.");
            }


            return beer;
        }
    }

}

